# hycon-assembly README

This is the README of hycon-HY08A series assembly syntax coloring extension.

## Features

This extension generally colors the generated .asm/.lst/.rst of HYCON 8-bit C compiler.

The compiler (IDE) can be found at 

http://www.hycontek.com/hy_mcu/HY11P-CIDEV1_2.zip

The colored version make them more easy to read.


**Enjoy!**
